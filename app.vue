<script setup lang="ts">
import { ulid, decodeTime} from "ulid";
import ClipboardJS from "clipboard";

let identifier = ref(ulid());

const timestamp = computed(() => {
    try {
        return decodeTime(identifier.value);
    } catch {
        return null;
    }
});

const timestampISO = computed(() => {
    if (timestamp.value === null) {
        return null;
    }
    try {
        return new Date(timestamp.value).toISOString();
    } catch {
        return null;
    }
});

const regenerate = () => {
    identifier.value = ulid();
}

onMounted(() => {
    new ClipboardJS('.btn-clipboard');
})

const meta = {
    title: 'ulid.page',
    description: 'Generate ULIDs and decode their timestamps',
    keywords: 'ULID, Universally Unique Lexicographically Sortable Identifier, generate, timestamp, unique identifier, UUID',
    baseUrl: 'https://ulid.page'
}
</script>

<template>
    <Html lang="en">
    <Head>
        <Title>{{ meta.title }}</Title>

        <Meta name="description" :content="meta.description" />
        <Meta name="keywords" :content="meta.keywords" />
        <Meta name="author" content="Andrea Vos <andrea@avris.it>" />

        <Meta property="og:type" content="article" />
        <Meta property="og:title" :content="meta.title" />
        <Meta property="og:url" :content="meta.baseUrl" />
        <Meta property="og:image" :content="`${meta.baseUrl}/banner.png`" />

        <Meta name="twitter:card" content="summary_large_image"/>
        <Meta name="twitter:title" :content="meta.title"/>
        <Meta name="twitter:description" :content="meta.description"/>
        <Meta name="twitter:image" :content="`${meta.baseUrl}/banner.png`"/>

        <Link rel="icon" href="./favicon.svg" sizes="any" type="image/svg+xml"/>

        <Script defer data-domain="ulid.page" src="https://plausible.avris.it/js/script.js"></Script>
    </Head>
    </Html>
    <div id="app">
        <header>
            <h1>ulid.page</h1>
            <p>Universally Unique Lexicographically Sortable Identifier</p>
        </header>
        <main>
            <div class="input-group">
                <input v-model="identifier" id="identifier" class="bigger"/>
                <button @click="regenerate" title="Generate new" class="bigger">🔄</button>
                <button title="Copy to clipboard" class="btn-clipboard bigger" data-clipboard-target="#identifier">📋</button>
            </div>
            <div class="stacked-columns">
                <div>
                    <label>Decoded time <small>(ISO)</small></label>
                    <div class="input-group">
                        <input v-model="timestampISO" id="timestampISO" readonly placeholder="---malformed ulid---"/>
                        <button v-if="timestampISO" title="Copy to clipboard" class="btn-clipboard" data-clipboard-target="#timestampISO">📋</button>
                    </div>
                </div>
                <div>
                    <label>Decoded time as unix timestamp <small>(milliseconds)</small>:</label>
                    <div class="input-group">
                        <input v-model="timestamp" id="timestamp" readonly placeholder="---malformed ulid---"/>
                        <button v-if="timestamp" title="Copy to clipboard" class="btn-clipboard" data-clipboard-target="#timestamp">📋</button>
                    </div>
                </div>
            </div>
        </main>
        <main>
            <div class="stacked-columns">
                <div>
                    <h2>
                        UUID can be suboptimal for many use-cases because:
                    </h2>
                    <ul class="mb-sm">
                        <li>It isn't the most character efficient way of encoding 128 bits of randomness</li>
                        <li>UUID v1/v2 is impractical in many environments, as it requires access to a unique, stable MAC address</li>
                        <li>UUID v3/v5 requires a unique seed and produces randomly distributed IDs, which can cause fragmentation in many data structures</li>
                        <li>UUID v4 provides no other information than randomness which can cause fragmentation in many data structures</li>
                    </ul>
                </div>
                <div>
                    <h2>
                        Instead, herein is proposed ULID:
                    </h2>
                    <ul class="mb-sm">
                        <li>128-bit compatibility with <a href="https://www.uuidgenerator.net/" target="_blank" rel="noopener">UUID</a></li>
                        <li>1.21e+24 unique ULIDs per millisecond</li>
                        <li>Lexicographically sortable!</li>
                        <li>Canonically encoded as a 26 character string, as opposed to the 36 character UUID</li>
                        <li>Uses <a href="https://en.wikipedia.org/wiki/Base32#Crockford%27s_Base32" target="_blank" rel="noopener">Crockford's base32</a> for better efficiency and readability (5 bits per character)</li>
                        <li>Case insensitive</li>
                        <li>No special characters (URL safe)</li>
                        <li>Monotonic sort order (correctly detects and handles the same millisecond)</li>
                    </ul>
                </div>
            </div>
        </main>
        <main>
            <p class="text-center">
                <a href="https://github.com/ulid/spec" class="button" target="_blank" rel="noopener">check out the specs</a>
            </p>
        </main>
        <footer>
            <p>
                Website by <a href="https://avris.it" target="_blank" rel="noopener">Andrea Vos</a>.
                I'm not the author of ULID itself, just a fan 😉
            </p>
            <p>
                Big kudos to whoever created
                <a href="https://web.archive.org/web/20230613190646/https://ulid.page/">the original ulid.page</a>.
                Looks like the domain expired and the project was abandoned.
                I was using it regularly to quickly decode ULID timestamps,
                so I decided to step up and keep the page alive.
            </p>
        </footer>
    </div>
</template>

<style>
@import url('https://fonts.googleapis.com/css2?family=DM+Mono&family=Noto+Emoji&display=swap');

:root {
    --color-primary: #0b0d97;
    --color-light: #f7f6ff;
    --color-dark: #040528;
    --color-grey: #dcdfec;
    --spacer: 2rem;
    --container-width: 1024px;
    --breakpoint: 1024px; /* doesn't work in @media… */
    --font-size: 16px;
    --font-family: 'DM Mono', 'Noto Emoji', monospace;
    --radius: .5rem;
}

:root {
    font-size: var(--font-size);
    font-family: var(--font-family);
}
@media (max-width: 1024px) {
    :root {
        --font-size: 14px;
    }
}

html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body {
    margin: 0;
}
img {
    max-width: 100%;
    height: auto;
}

#app {
    background: var(--color-light);
    color: var(--color-dark);
    min-height: 100vh;
    display: flex;
    flex-direction: column;
}

header, main, footer {
    padding-block: var(--spacer);
    padding-inline: max(var(--spacer), calc((100vw - var(--container-width))/2));
}

header h1 {
    font-size: calc(4 * var(--font-size));
}

main {
    flex-grow: 1;
}

footer {
    background: var(--color-dark);
    color: var(--color-light);
    font-size: 0.75em;
}
footer a {
    color: var(--color-light);
}

a {
    color: var(--color-primary);
}

input {
    font-family: var(--font-family);
    background-color: var(--color-light);
}
input[readonly], input[disabled] {
    background-color: var(--color-grey);
}
button, .button {
    background-color: var(--color-primary);
    color: var(--color-light);
    border-radius: var(--radius);
    cursor: pointer;
    text-decoration: none;
    display: inline-block;
    font-family: var(--font-family);
}
button:hover, .button:hover {
    filter: brightness(125%);
}
.input-group {
    display: flex;
    margin-block-end: calc(var(--spacer) / 2);
    width: 100%;
}
.input-group input {
    flex-grow: 1;
}
.input-group input, button, .button {
    padding: calc(var(--spacer) / 3) calc(var(--spacer) / 2);
}
.input-group > * {
    border: 1px solid var(--color-primary);
    border-radius: 0;
}
.input-group > *:not(:last-child) {
    border-inline-end: none;
}
.input-group > *:first-child {
    border-top-left-radius: var(--radius);
    border-bottom-left-radius: var(--radius);
}
.input-group > *:last-child {
    border-top-right-radius: var(--radius);
    border-bottom-right-radius: var(--radius);
}

.stacked-columns {
    display: flex;
    flex-direction: column;
}
@media (max-width: 1024px) {
    .stacked-columns {
        margin-block-start: calc(var(--spacer) / 4);
    }
}
@media (min-width: 1024px) {
    .stacked-columns {
        flex-direction: row;
    }
    .stacked-columns > * {
        width: 100%;
    }
    .stacked-columns > *:not(:first-child) {
        margin-inline-start: calc(var(--spacer) / 4);
    }
    .stacked-columns > *:not(:last-child) {
        margin-inline-end: calc(var(--spacer) / 4);
    }
}
input.bigger, button.bigger, .button.bigger {
    font-size: 1.05em;
}

h1, h2, h3, h4, h5, h6, p, ol, ul {
    margin-block-start: 0;
    margin-block-end: calc(var(--spacer) / 4);
}
.mb {
    margin-block-end: var(--spacer);
}
@media (max-width: 1024px) {
    .mb-sm {
        margin-block-end: var(--spacer);
    }
}
ol, ul {
    padding-left: var(--spacer);
}
ol, ul > li {
    margin-block-end: calc(var(--spacer) / 4);
}

.text-center {
    text-align: center;
}
</style>
