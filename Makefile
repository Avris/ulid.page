install:
	pnpm install

dev: install
	pnpm run dev

deploy: install
	pnpm run generate

